
/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {
public:
	bool operator == (Circle c1) {
		if (r == c1.getR())
			return true;
		else
			return false;
	}
	Circle(double);
	virtual ~Circle();
	void setR(double);
	double getR() const ;
	double calculateCircumference() const ;
	double calculateArea();
	void setR(int);
private:
	double r;
	const double PI = (double) 22/7;
};
#endif /* CIRCLE_H_ */
