/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"

	Circle::Circle(double r) {
		setR(r);
	}

	Circle::~Circle() {
	}


	void Circle::setR(double r) {
		this->r = r;
	}

	double Circle::getR() const {
		return r;
	}

	double Circle::calculateCircumference() const{
		return PI * r * 2; //wrong circle circumference fixed
	}

	double Circle::calculateArea() {
		return PI * r * r; // wrong circle area fixed
	}

	void Circle::setR(int r) {
		this->r = r;
	}